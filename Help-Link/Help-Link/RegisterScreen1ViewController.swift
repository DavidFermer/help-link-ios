//
//  RegisterScreen1ViewController.swift
//  Help-Link
//
//  Created by David Fermer on 8/13/16.
//  Copyright © 2016 Factotum Mobile Technologies. All rights reserved.
//

import UIKit

class RegisterScreen1ViewController: UIViewController, GIDSignInUIDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
		
		GIDSignIn.sharedInstance().uiDelegate = self
		
		/// Uncomment to automatically sign in the user.
		//GIDSignIn.sharedInstance().signInSilently()
		
		// TODO(developer) Configure the sign-in button look/feel
		// ...
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
